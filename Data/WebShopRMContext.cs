﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using WebShopRM.Models;

namespace WebShopRM.Models
{
    public class WebShopRMContext : DbContext
    {
        public WebShopRMContext( DbContextOptions<WebShopRMContext> options)
            : base(options)
        {
        }
       // private static readonly LoggerFactory Logger
       //= new LoggerFactory(new[] { new ConsoleLoggerProvider((_, __) => true, true) });


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder
            //    .UseLoggerFactory(Logger);
                //.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Test;ConnectRetryCount=0");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductCategory>()
                .HasKey(pc => new { pc.ProductID, pc.CategoryID });
            modelBuilder.Entity<ProductCategory>()
                .HasOne<Product>(pc => pc.Product)
                .WithMany(p => p.ProductCategory)
                .HasForeignKey(pc => pc.ProductID);
            modelBuilder.Entity<ProductCategory>()
                .HasOne<Category>(pc => pc.Category)
                .WithMany(c => c.ProductCategory) //.WithMany(p => p.ProductCategory)
                .HasForeignKey(pc => pc.CategoryID);


            modelBuilder.Entity<ProductPicture>()//pb=produktBild
               .HasKey(pb => new { pb.ProductID, pb.PictureID });
            modelBuilder.Entity<ProductPicture>()
                .HasOne<Product>(pb => pb.Product)
                .WithMany(p => p.ProductPicture)
                .HasForeignKey(pb => pb.ProductID);
            modelBuilder.Entity<ProductPicture>()
                .HasOne<Picture>(pb => pb.Picture)
                .WithMany(pi => pi.ProductPicture)
                .HasForeignKey(pb => pb.PictureID);

        }

        public DbSet<WebShopRM.Models.Product> Product { get; set; }

        public DbSet<WebShopRM.Models.Picture> Picture { get; set; }

        public DbSet<WebShopRM.Models.Category> Category { get; set; }

        public DbSet<CartItem> CartItems { get; set; }

        public DbSet<ProductCategory> ProductCategory { get; set; }

        public DbSet<ProductPicture> ProductPicture { get; set; }
    }
}

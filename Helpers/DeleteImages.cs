﻿using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.Hosting.IWebHostEnvironment : IHostEnvironment

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Helpers
{
    public class DeleteImages : IDeleteImages
    {
        //[Obsolete]
        //private readonly IHostingEnvironment _hostingEnv;
        private readonly IWebHostEnvironment _webHostEnv;

        //[Obsolete]
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1041:Provide ObsoleteAttribute message", Justification = "<Pending>")]
        public DeleteImages(IWebHostEnvironment hostingEnv)//IHostingEnvironment hostingEnv)
        {
            //_hostingEnv = hostingEnv ?? throw new ArgumentNullException(nameof(hostingEnv));
            _webHostEnv = hostingEnv ?? throw new ArgumentNullException(nameof(hostingEnv));
        }

        public void Delete(string path, string partialFileName)
        {
            string webRoot = _webHostEnv.WebRootPath;//wwwroot

            if (path == null || path == "")
            {
                System.Diagnostics.Debug.WriteLine("hittar inte sökvägen");
                return;
            };

            if (Directory.Exists(Path.Combine(webRoot, path + "/")))
            {
                var _fullPath = Path.Combine(webRoot, path + "/");

                string[] fileList = Directory.GetFiles(_fullPath, "*" + partialFileName + "*");
                foreach (string file in fileList)
                {
                    //todo: Make better dialog to user
                    System.Diagnostics.Debug.WriteLine(file + " kommer att raderas");
                    System.IO.File.Delete(file);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Hittar inte platsen där bilderna finns");


            }


        }
    }
}

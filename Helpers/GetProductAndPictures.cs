﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShopRM.Models;


namespace WebShopRM.Helpers
{
    public class GetProductAndPictures
    {
        private readonly WebShopRMContext _context;

        public GetProductAndPictures(WebShopRMContext context)
        {
            _context = context;
        }
        public struct ProductAndPicture
        {
             public Product product { get; set; }
             public Picture picture { get; set; }
        }

        public async Task <List<Product>> GetCategoryProducts(int catid)
        {
            var CategoryProductList = new List<Product>();

            CategoryProductList = await _context.ProductCategory
                .Where(pc => pc.CategoryID == catid)
                .Select(pc => pc.Product).ToListAsync().ConfigureAwait(false);
            
            return CategoryProductList;
        }

        public async Task<List<Product>> GetAllProductsByCategory()
        {
            var AllProductByCategory = new List<Product>();

            var temp = await _context.Product
                .Include(pc => pc.ProductCategory)
                .ThenInclude(pc => pc.Category).ToListAsync().ConfigureAwait(false);
                


            return AllProductByCategory;
        }

        public IQueryable GetIQuerableProductAndPictureList()
        {

            var _productAndPictures = new List<ProductAndPicture>();

            foreach (Product p in _context.Product)
            {

                var piid = _context.ProductPicture.Where(pp => pp.ProductID == p.Id)
                                                .Select(pp => pp.PictureID).FirstOrDefault();

                

                var test = _context.Product
                    .Include(i => i.ProductPicture)
                        .ThenInclude(i => i.PictureID).FirstOrDefault();
                //var test1 = test.ProductPicture.PictureID;

                //assign temp image if none to find
                if (piid != 0)
                {
                    _productAndPictures.Add(new ProductAndPicture
                    {
                        product = p,

                        picture = _context.Picture.Where(x => x.PictureID == piid).FirstOrDefault()

                    });
                }
                else
                {
                    _productAndPictures.Add(new ProductAndPicture
                    {
                        product = p,

                        picture = _context.Picture.Where(x => x.PictureID == 1).FirstOrDefault()

                    });
                }
            }

            return _productAndPictures.AsQueryable();
        }

        public void GetPicturesByProductId(int productId)
        {

            var PictureList = new List<Picture>();
            var pictures = _context.Picture;

            foreach (Product p in _context.Product)
            {

                var piid = _context.ProductPicture.Where(pp => pp.ProductID == productId)
                                                .Select(pp => pp.PictureID);
                //assign temp image if none to find
                //if (piid != 0)
                //{
                //    var _picture = from x in picture
                //                  Where x.PictureID == piid
                //                    Select x;

                //    PictureList.Add(picture);
                //}
                //else
                //{
                //    _productAndPictures.Add(new ProductAndPicture
                //    {
                //        product = p,

                //        picture = _context.Picture.Where(x => x.PictureID == 1).FirstOrDefault()

                //    });
                //}


                //return PictureList;
            }
        }
    }
}

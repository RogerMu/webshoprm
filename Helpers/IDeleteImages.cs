﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Helpers
{
    public interface IDeleteImages
    {
        void Delete(string path, string name);
    }
}
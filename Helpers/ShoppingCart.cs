﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShopRM.Models;

namespace WebShopRM.Helpers
{
    public partial class ShoppingCart 
    {
        private readonly WebShopRMContext _context;
        public ShoppingCart(WebShopRMContext context) 
        {
            _context = context;
        }
        public ShoppingCart(){}//empty constructor
        string ShoppingCartId { get; set; }
        public const string CartSessionKey = "CartId";
        //public static ShoppingCart GetCart(HttpContext contextBase)
        //{
        //    var cart = new ShoppingCart();
        //    cart.ShoppingCartId = cart.GetCartId(contextBase);
        //    return cart;
        //}
        //HttpContextBase to allow access to cookies
        public string GetCartId(HttpContext contextBase)
        {
            if (contextBase.Session.GetString(CartSessionKey) == null)
                //if (contextBase.Session[CartSessionKey] == null)

            {
                if (!string.IsNullOrWhiteSpace(contextBase.User.Identity.Name))
                {
                    //contextBase.Session[CartSessionKey] = contextBase.User.Identity.Name;
                    contextBase.Session.SetString(CartSessionKey, contextBase.User.Identity.Name);
                }
                else
                {
                    Guid tempCartId = Guid.NewGuid();
                    //contextBase.Session[CartSessionKey] = tempCartId.ToString();
                    contextBase.Session.SetString(CartSessionKey, tempCartId.ToString());
                }
            }
            //return contextBase.Session[CartSessionKey].ToString();
            return contextBase.Session.GetString(CartSessionKey);
        }
        //public static ShoppingCart GetCart(Controller controller)
        //{
        //    return GetCart(controller.HttpContext);
        //}

        public void AddToCart(Product product)
        {

        }

        //public int RemoveFromCart(int id)
        //{
        //   var quantity = _context.CartItem.Single(cartItem.ItemId=>CartItem.ItemId == ShoppingCart.ShoppingCartId)
        //   return quantity;
        //}

        //ny inlånad kod https://stackoverflow.com/questions/34284486/c-sharp-mvc-simple-cart-based-on-session
        //public ActionResult AddToBasket(int? id)
        //{
        //    if (Session["Basket"] == null)
        //    {
        //        Session["Basket"] = new List<int>();
        //    }

        //    var items = (List<int>)Session["Basket"];
        //    items.Add(id.Value);
        //    Session["Basket"] = items;
        //    ViewBag.List = Session["Basket"];

        //    return RedirectToAction("Index");
        //}

        
    }
}

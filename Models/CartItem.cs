﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    public class CartItem
    {
        [Key]
        public string ItemId { get; set; }

        //public string CartId { get; set; }

        public int Quantity { get; set; }

        public System.DateTime DateCreated { get; set; }

        public int ProductId { get; set; }

        public Product Product { get; set; }  //removed virtual?
    }
}

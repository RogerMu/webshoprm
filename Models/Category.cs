﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    public class Category
    {
        [ScaffoldColumn(false)]
        public int CategoryID { get; set; }

        [Required, StringLength(100), Display(Name = "Category name")]
        public string CategoryName { get; set; }

        [Display(Name = "Category description")]
        public string Description { get; set; }

        public virtual ICollection<ProductCategory> ProductCategory { get; set; }
    }
}

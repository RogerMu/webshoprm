﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    public class Contact
    {
        public Contact()
        {
            CreatedOn = DateTimeOffset.Now;
        }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string EmailAddress { get; set; }

        [StringLength(450)]
        public string Address { get; set; }

        public string Content { get; set; }

        //public long ContactAreaId { get; set; }

        //public virtual ContactArea ContactArea { get; set; }

        public bool IsDeleted { get; set; }

        public DateTimeOffset CreatedOn { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    public class Discount
    {
      [Key]
      [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      public int Id { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public decimal? Amount { get; set; }
      public int? Percentage { get; set; }
      [Required]
      public DateTime BeginDiscount { get; set; }
      [Required]
      public DateTime EndDiscount { get; set; }
      public ICollection<Category> Category { get; set; }
      public ICollection<Product> Product { get; set; }

      //[Column(TypeName = "decimal(18,2)")]
      //public List<decimal> OldPrice { get; set; } 
    }
}

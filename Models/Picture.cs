﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    public class Picture
    {
        public int PictureID { get; set; }
        public string PicName { get; set; }
        public string PicSizes { get; set; }
        public string PicSizeOriginal { get; set; }
        public string PicPath { get; set; } 
        public DateTime Updated { get; set; }


#pragma warning disable CA2227 // Collection properties should be read only
        public ICollection <ProductPicture> ProductPicture { get; set; }
#pragma warning restore CA2227 // Collection properties should be read only
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebShopRM.Models
{
    public class Product
    {
        //[ScaffoldColumn(false)]
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, StringLength(100), Display(Name = "Name")]
        public string ProductName { get; set; }
        [StringLength(100)]
        public string PartNumber { get; set; }
        [StringLength(50)]
        public string ISBN { get; set; }

        [Required, StringLength(10000), Display(Name = "Product Description"), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string ImagePath { get; set; }

        [Display(Name = "Price")]
        //[DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Price { get; set; }

        public int StockQuantity { get; set; } = 0;

        public bool Visible { get; set; }

        //[DisplayFormat(DataFormatString ="{0:yyyyMMdd HH:mm")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? Updated { get; set; }

        [Column(TypeName = "deciaml(5,3)")]
        public decimal Vat  { get; set; }

        //public int? CategoryID { get; set; }
        //public string Freight {get;set;}
        //publig int Weight {get;set;}

        public ICollection<ProductCategory> ProductCategory { get; set; }

        public ICollection<ProductPicture> ProductPicture { get; set; }
    }
}

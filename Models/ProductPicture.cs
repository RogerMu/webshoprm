﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    public class ProductPicture
    {
        public int ProductID { get; set; }
        public Product Product { get; set; }

        public int PictureID { get; set; }
        public Picture Picture { get; set; }
    }
}

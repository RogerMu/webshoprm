﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
//using Microsoft.AspNetCore.Mvc.IActionResult;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Helpers;
using WebShopRM.Models;
using Microsoft.AspNetCore.Http;
using WebShopRM.Pages;
using WebShopRM.Pages.Products;

namespace WebShopRM.Pages.Cart
{
    public class CartModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;
        //private readonly object ViewBag;

        public CartModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }
        //public CartItem cartItem { get; set; }

        public List<CartItem> cart { get; set; }
        public double Total { get; set; }

        public Product product { get; set; }

        [BindProperty]
        public int Id { get; set; }
        public void OnGet()
        {
            cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            decimal a = cart.Sum(i => i.Product.Price * i.Quantity).Value;
            Total = Decimal.ToDouble(a);
        }
        
        public IActionResult OnGetBuy(int id)
        {
            cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            if (cart == null)
            {
                cart = new List<CartItem>();
                cart.Add(new CartItem
                {
                    Product = _context.Product.Find(id),
                    Quantity = 1
                });
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            else
            {
                int index = Exists(cart, id);
                if (index == -1)
                {
                    cart.Add(new CartItem
                    {
                        Product = _context.Product.Find(id),
                        Quantity = 1
                    });
                }
                else
                {
                    cart[index].Quantity++;
                }
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            return RedirectToPage("Cart");
        }

        public IActionResult OnGetRemove(int id)
        {
            cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            int index = Exists(cart, id);
            cart.RemoveAt(index);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToPage("Cart");
        }

        public IActionResult OnPostUpdate(int [] quantities)
        {
            cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            for (var i =0;i<cart.Count;i++)
            {
                cart[i].Quantity = quantities[i];
            }
            return RedirectToPage("Cart");
        }

        private int Exists(List<CartItem> cart, int id)
        {
            for (var i = 0; i < cart.Count; i++)
            {
                if (cart[i].Product.Id == id)
                {
                    return i;
                }
            }
            return -1;
        }



        //public IActionResult OnGet()//rename
        //{
        //    //todo move this method to own place
        //    var cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
        //    ViewData["cart"] = cart;
        //    ViewData["total"] = cart.Sum(item => item.Product.Price * item.Quantity);

        //    return Page();
        //}

        //[Route("buy/{id}")]
        //public ActionResult Buy(int id)
        //public IActionResult OnPostBuy(int id)
        //{
        //    if (SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart") == null)
        //    {
        //        List<CartItem> cart = new List<CartItem>();
        //        cart.Add(new CartItem { Product = _context.Product.Find(id), Quantity = 1 });
        //        SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
        //    }
        //    else
        //    {
        //        List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
        //        int index = isExist(id);
        //        if (index != -1)
        //        {
        //            cart[index].Quantity++;
        //        }
        //        else
        //        {
        //            cart.Add(new CartItem { Product = _context.Product.Find(id), Quantity = 1 });
        //        }
        //        SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
        //    }
        //    return RedirectToAction("index");
        //}


        public IActionResult Remove(int id)
        {
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            int index = isExist(id);
            cart.RemoveAt(index);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction("Index");
        }

        private int isExist(int id)
        {
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].Product.Id.Equals(id))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
    //    //ChienVH https://www.youtube.com/watch?v=h_jfcLICk_8
    //    //ShoppingCart "controller"
    //    private string strCart = "Cart";
    //    //Get: ShoppingCart 
    //    public ActionResult Index()
    //    {
    //        return Index();//View();
    //    }
    //    public ActionResult OrderNow(int? id)
    //    {
    //        if (id == null)
    //        {
    //            //return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
    //            return BadRequest();
    //        }
    //        if (HttpContext.Session.GetString(strCart) == null)
    //        {
    //            List<CartItem> lsCart = new List<CartItem>();

    //            //new CartItem ( _context.Product.Find(id, 1)) //fixa konstruktorn?
    //            lsCart.Add(new CartItem { Product = _context.Product.Find(id), Quantity = 1 });
    //            SessionHelper.SetObjectAsJson(HttpContext.Session, "stCart", lsCart);
    //            //List<CartItem> cart = new List<CartItem>();
    //            //    cart.Add(new CartItem { Product = _context.Product.Find(id), Quantity = 1 });
    //            //    SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);



    //        }
    //        else
    //        {
    //            List<CartItem> lsCart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "strCart");
    //            int check = isExist(id);
    //            if (check != -1)
    //                lsCart[check].Quantity++;
    //            else
    //                lsCart.Add(new CartItem { Product = _context.Product.Find(id), Quantity = 1 });

    //            SessionHelper.SetObjectAsJson(HttpContext.Session, "strCart", lsCart);
    //        }
    //        return Index(); //View();
    //    }
    //}
    //private int IsExist(string id)//looking for double items
    //{
    //    //List<CartItem> lsCart = (List<CartItem>)Session[strCart];
    //    List<CartItem> lsCart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "lsCart");
    //    for (int i = 0; i < lsCart.Count; i++)
    //    {
    //        if (lsCart[i].Product.Id.Equals(id))
    //        {
    //            return i;
    //        }
    //    }
    //    return -1;
    //}
//}
    

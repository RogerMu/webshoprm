﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Helpers;
using WebShopRM.Models;

namespace WebShopRM.Pages.Pictures
{
    
    public class CreateModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;
        private IHostingEnvironment _env; //håller reda på sökvägar

        public CreateModel(WebShopRM.Models.WebShopRMContext context, IHostingEnvironment env)
        {
            _context = context;
            _env = env;
        }
        [BindProperty]
        public Picture picture { get; set; }
        
        public IActionResult OnGet()
        {
            //PicturesViewModel picVM = new PicturesViewModel();
            return Page();
        }


        [BindProperty]
        public IFormFile UploadFile { get; set; }
        [BindProperty]
        public string PicName { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var fileExtension = Path.GetExtension(UploadFile.FileName);
            string pictureFolderPath = ImagePathBuilder.NewPath(_env.WebRootPath);
            var picturePath = (Path.Combine(pictureFolderPath, (Guid.NewGuid().ToString())))+ fileExtension;
            
            using (FileStream stream = new FileStream(picturePath, FileMode.Create))
            {

                //await imgFile.CopyToAsync(stream);
                await UploadFile.CopyToAsync(stream);
            }

            int startPosToSelect = picturePath.IndexOf("wwwroot\\Upload", System.StringComparison.CurrentCultureIgnoreCase);
            string relativePicturePath = picturePath.Substring(startPosToSelect + 8); //cut after "wwwroot/"

            Picture picture = new Picture
            {
                PicPath = relativePicturePath,
                PicName = PicName,
                Updated = DateTime.UtcNow
            };


            _context.Picture.Add(picture);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
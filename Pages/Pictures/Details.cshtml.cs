﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;

namespace WebShopRM.Pages.Pictures
{
    public class DetailsModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;

        public DetailsModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }

        public Picture Picture { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Picture = await _context.Picture.FirstOrDefaultAsync(m => m.PictureID == id);

            if (Picture == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}

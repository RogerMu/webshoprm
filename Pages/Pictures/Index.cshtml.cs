﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;

namespace WebShopRM.Pages.Pictures
{
    public class IndexModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;

        public IndexModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }

        public IList<Picture> Picture { get;set; }

        public async Task OnGetAsync()
        {
            Picture = await _context.Picture.ToListAsync().ConfigureAwait(false);
        }
    }
}

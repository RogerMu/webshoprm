﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query;
using WebShopRM.Helpers;
using WebShopRM.Models;
using WebShopRM.Pages.Cart;

namespace WebShopRM.Pages.Products
{
    public class CatalogModel : PageModel
    {
        private readonly WebShopRMContext _context;
        public CatalogModel(WebShopRMContext context)
        {
            _context = context;
        }

        public string NameSort { get; set; }
        public string PriceSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        public PaginatedList<Product> Products { get; set; }

        [BindProperties]
        public class ProductAndPicture
        {
            public Product product { get; set; }
            public Picture picture { get; set; } 
        }
        [BindProperty]
        public IQueryable<ProductAndPicture> productAndPictures { get; set; }


        public async Task OnGetAsync(string sortOrder,
        string currentFilter, string searchString, int? pageIndex)
        {
            var _productAndPictures = new List<ProductAndPicture>();

            foreach (Product p in _context.Product)
            {
                var piid = _context.ProductPicture.Where(pp => pp.ProductID == p.Id)
                                                .Select(pp => pp.PictureID).FirstOrDefault();
                if (piid != 0 )
                {
                    _productAndPictures.Add(new ProductAndPicture
                    {
                        product = p,
                        picture = _context.Picture.Where(x => x.PictureID == piid).FirstOrDefault()
                    });
                }
                else
                {
                    //assign temp image if none to find
                    _productAndPictures.Add(new ProductAndPicture
                    {
                        product = p,

                        picture = _context.Picture.Where(x => x.PictureID == 1).FirstOrDefault()

                    });
                }         
            }
            productAndPictures = _productAndPictures.AsQueryable();


            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name" : "";
            PriceSort = sortOrder == "price" ? "price_h" : "price";

            //
            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            //

            CurrentFilter = searchString;

            IQueryable<Product> productPrice = from s in _context.Product select s;
            //IQueryable<ProductAndPicture> productPrice = from s in productAndPictures select s;
            

            if (!String.IsNullOrEmpty(searchString))
            {
                productPrice = productPrice.Where(s => s.ProductName.Contains(searchString));
                //|| s.FirstMidName.Contains(searchString)) ; 
            }

            switch (sortOrder)
            {
                case "name":
                    productPrice = productPrice.OrderByDescending(s => s.ProductName);
                    break;
                case "price":
                    productPrice = productPrice.OrderBy(s => s.Price);
                    break;
                case "price_h":
                    productPrice = productPrice.OrderByDescending(s => s.Price);
                    break;
                default:
                    productPrice = productPrice.OrderBy(s => s.ProductName);
                    break;
            }

            int pageSize = 4;
            //Products = await productPrice.AsNoTracking().ToListAsync().ConfigureAwait(false);
            Products = await PaginatedList<Product>.CreateAsync(productPrice.AsNoTracking(), pageIndex ?? 1, pageSize).ConfigureAwait(false);
            
        }

    }
}
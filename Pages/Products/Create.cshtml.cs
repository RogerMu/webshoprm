﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebShopRM.Models;

namespace WebShopRM.Pages.Products
{
    public class CreateModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;
        //private readonly object ViewBag;

        public CreateModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }
        //wm top

        [BindProperty]
        public Product Product { get; set; }
        
        [BindProperty]
        [Required, StringLength(100), Display(Name = "Name")]
        public string ProductName { get; set; }

        [BindProperty]
        [StringLength(100)]
        public string PartNumber { get; set; }
        [BindProperty]
        [StringLength(50)]
        public string ISBN { get; set; }
        [BindProperty]
        [Required, StringLength(10000), Display(Name = "Product Description"), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //public string ImagePath { get; set; }

        [BindProperty]
        [Display(Name = "Price")]
        //[DataType(DataType.Currency)]
        //[Column(TypeName = "decimal(18,2)")]
        public decimal? Price { get; set; }

        [BindProperty]
        //[DisplayFormat(DataFormatString ="{0:#,###}")]
        public decimal Tax { get; set; } = 25;

        [BindProperty]
        public int StockQuantity { get; set; }
        [BindProperty]
        public bool Visible { get; set; }

        [DisplayFormat(DataFormatString ="{0:yyyyMMdd HH:mm")]
        public DateTime? Updated { get; set; }

        [BindProperty]
        public List<Category> AllCategories { get; set; }
        

        [BindProperty]
        public int[] SelectedCategories { get; set; }

        public ICollection<int> CatIds { get; set; }

        public ICollection<Picture> Picture { get; set; }
       

        public IActionResult OnGet()
        //public IActionResult Create()
        {
            AllCategories = (from c in _context.Category
                                 orderby c.CategoryName
                                 select new
                                 {
                                     CategoryName = c.CategoryName,
                                     CategoryID = c.CategoryID
                                 }).AsEnumerable()
                                    .Select(c => new Category()
                                    {
                                        CategoryID = c.CategoryID,
                                        CategoryName = c.CategoryName
                                    }).ToList();

            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            Product product = new Product();

            if (!ModelState.IsValid)
            {
                return Page();
            }

            product.Updated = DateTime.UtcNow;
            product.ProductName = ProductName;
            product.Price = Price;
            //var decimalFormat = Tax.ToString("#####,###");
            product.Vat = Math.Round(Convert.ToDecimal(Tax), 3);
            product.PartNumber = PartNumber;
            product.ISBN = ISBN;
            product.Description = Description;
            product.StockQuantity = StockQuantity;
            product.Visible = Visible;


            _context.Product.Add(product);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            //After the Product with its properties are saved
            //we need to save categories that belongs to this product
            //that are saved into the connection table

            var c = SelectedCategories.Count();

            var productCategory = new List<ProductCategory>();

            for (var i = 0; i < c; i++)
            {
                productCategory.Add(new ProductCategory
                {
                    CategoryID = SelectedCategories[i],
                    ProductID = product.Id
                });
                
            }

            //product.ProductCategory = _product.ProductCategory;

            _context.ProductCategory.AddRange(productCategory);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
        
    }
}
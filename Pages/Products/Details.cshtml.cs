﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;

namespace WebShopRM.Pages.Products
{
    public class DetailsModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;

        public DetailsModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }

        public Product Product { get; set; }

        //[BindProperty]
        public List<Picture> ThisProductPictures { get; set; }

        IList<Category>categories{ get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Product = await _context.Product.FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);

            var thisProductPictures = _context.Picture.Where(s => s.ProductPicture.Any(i => i.ProductID == id)).ToListAsync();


            IQueryable<Category> categories = from c in _context.ProductCategory
                         where c.ProductID == id
                         select c.Category;

            ViewData["Details"] = categories;
            //ViewData["Pictures"] = thisProductPictures;
            ThisProductPictures = await thisProductPictures.ConfigureAwait(false);

            if (Product == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;

namespace WebShopRM.Pages.Products
{
    //[BindProperties]
    //public class EditAndAddPictureVM:PageModel
    //{
    //    public Product Product { get; set; }
    //    public IList<Picture> Pictures { get; set; }
    //    public List<bool> IsSelected { get; set; }
    //}
    public class EditModel : PageModel
    {
        private  WebShopRM.Models.WebShopRMContext _context;

        public EditModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Product Product { get; set; }

        [BindProperty]
        public List<Category> AllCategories { get; set; }

        [BindProperty]
        public int[] SelectedCategories { get; set; }


        [BindProperty]
        public IList<Picture> Pictures { get; set; }

        public IList<ProductPicture> ProductPictures { get; set; }

        [BindProperties]
        public class PicItem
        {
            public Picture picture { get; set; }
            public bool selectPic { get; set; }
        }
        [BindProperty]
        public IList<PicItem> LstPicItems { get; set; }
       
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SelectedCategories = await _context.Category.Where(x => x.ProductCategory.Any(i=>i.ProductID == id)).Select(r=>r.CategoryID).ToArrayAsync().ConfigureAwait(false);
            AllCategories = await _context.Category.ToListAsync().ConfigureAwait(false);


            Product = await _context.Product.FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false);
            Pictures = await _context.Picture.ToListAsync().ConfigureAwait(false);

            var thisProductPictures =  await _context.Picture.Where(s => s.ProductPicture.Any(i => i.ProductID == id)).ToListAsync().ConfigureAwait(false);

            var _lstPicItems = new List<PicItem>();

            //populate checkboxes with true and false checkboxes
            foreach (Picture p in _context.Picture)//causing craches if you come from index, async issues solved
            {
                if (thisProductPictures.Any(i => i.PictureID == p.PictureID))
                {
                     _lstPicItems.Add(new PicItem
                    {
                        picture = p,
                        selectPic = true
                    });
                }
                else 
                {
                    _lstPicItems.Add(new PicItem
                    {
                        picture = p,
                        selectPic = false
                    });
                };
            }
            LstPicItems = _lstPicItems;


            if (Product == null)
            {
                return NotFound();
            }
            return Page();
        }

        private void Exception(string e)
        {
            throw new NotImplementedException(e);
        }

        //On Post
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var selectedPicturesToSave = new List<Picture>();
            var product = new Product();
            
            //gather selected pictures
            for (int i = 0; i < LstPicItems.Count; i++)
            {
                if (LstPicItems[i].selectPic == true)
                {
                    selectedPicturesToSave.Add(LstPicItems[i].picture);
                }
            }
            //delete old selection of pictures. Needed before add new ones.
            var gatherOldProductPicture = await _context.ProductPicture.Where(x => x.Product.Id == this.Product.Id).Select(pi => pi.Picture).ToListAsync().ConfigureAwait(false);
            IList<ProductPicture> productPicturesToDelete = new List<ProductPicture>();
            if (gatherOldProductPicture.Count > 0 && !(gatherOldProductPicture == null))
            {
                for (var i = 0; i < gatherOldProductPicture.Count; i++)
                {
                    productPicturesToDelete.Add(new ProductPicture
                    {
                        PictureID = gatherOldProductPicture[i].PictureID,
                        ProductID = this.Product.Id
                    });
                }
            }
            _context.ProductPicture.RemoveRange(productPicturesToDelete);
            await _context.SaveChangesAsync().ConfigureAwait(false);
            //save to context here, do we need?

            //Save the new selection of pictures
            IList<ProductPicture> productPicturesToDB = new List<ProductPicture>();
            if (selectedPicturesToSave.Count>0 && !(selectedPicturesToSave == null))
            {
                for (var i = 0; i < selectedPicturesToSave.Count; i++)
                {
                    productPicturesToDB.Add(new ProductPicture
                    {
                        PictureID = selectedPicturesToSave[i].PictureID,
                        ProductID = this.Product.Id
                    });
                }       
            }
                _context.ProductPicture.AddRange(productPicturesToDB);
                await _context.SaveChangesAsync().ConfigureAwait(false);
            
            product.Description = Product.Description;
            product.ISBN = Product.ISBN;
            product.PartNumber = Product.PartNumber;
            product.Price = Product.Price;
            product.ProductName = Product.ProductName;
            product.StockQuantity = Product.StockQuantity;
            product.Vat = Product.Vat;
            product.Visible = Product.Visible;
            product.Updated = DateTime.UtcNow;
            product.Id = Product.Id;

            _context.Product.Attach(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(Product.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}

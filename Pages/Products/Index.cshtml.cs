﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;

namespace WebShopRM.Pages.Products
{
    public class IndexModel : PageModel
    {
        private readonly WebShopRM.Models.WebShopRMContext _context;

        public IndexModel(WebShopRM.Models.WebShopRMContext context)
        {
            _context = context;
        }
        public IList<Product> Product { get; set; }

        //public IQueryable<ProductAndPicture> productAndPictures { get; set; }

        public List<ProductAndPicture> productAndPictures { get; set; }

        public class ProductAndPicture
        {
            public Product product { get; set; }
            public Picture picture { get; set; }
        }
        public async Task OnGetAsync()//todo, clear up, make less querable, more stright forward
        {
            var _productAndPictures = new List<ProductAndPicture>();

            foreach (Product p in _context.Product)
            {
                var piid = _context.ProductPicture.Where(pp => pp.ProductID == p.Id)
                                                .Select(pp => pp.PictureID).FirstOrDefault();
                if (piid != 0)
                {
                    _productAndPictures.Add(new ProductAndPicture
                    {
                        product = p,
                        picture = _context.Picture.Where(x => x.PictureID == piid).FirstOrDefault()
                    });
                }
                else
                {   
                    //assign temp image if none to find
                    _productAndPictures.Add(new ProductAndPicture
                    {
                        product = p,
                        picture = _context.Picture.FirstOrDefault()
                    });
                }

            }

            productAndPictures = _productAndPictures;

            Product = await _context.Product.ToListAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;

namespace WebShopRM.Pages.Products
{
    public class PaginateModel : PageModel
    {
        private readonly WebShopRMContext _context;
         public PaginateModel(WebShopRMContext context)
        {
            _context = context;
        }

        public string NameSort { get; set; }
        public string PriceSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        public PaginatedList<Product> Products { get; set; }

        public async Task OnGetAsync(string sortOrder,
            string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name" : "";
            PriceSort = sortOrder == "price" ? "price_h" : "price";
            
            //
            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            //

            CurrentFilter = searchString;

            IQueryable<Product> productItem = from s in _context.Product
                                           select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    productItem = productItem.Where(s => s.ProductName.Contains(searchString));
                    //|| s.FirstMidName.Contains(searchString)) ; 
                }

                switch (sortOrder)
                {
                    case "name":
                        productItem = productItem.OrderByDescending(s => s.ProductName);
                        break;
                    case "price_h":
                        productItem = productItem.OrderByDescending(s => s.Price);
                        break;
                    case "price":
                        productItem = productItem.OrderBy(s => s.Price);
                        break;
                    default:
                        productItem = productItem.OrderBy(s => s.ProductName);
                        break;
                }

            int pageSize = 3;
            //Products = await productItem.AsNoTracking().ToListAsync().ConfigureAwait(false);
            Products = await PaginatedList<Product>.CreateAsync(productItem.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
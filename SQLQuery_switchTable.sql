﻿EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

Alter Table Product switch to Product2
drop table Product
exec sp_rename 'Product2', 'Product'
exec sp_msforeachtable @command1="print '?'",@command2="ALTER TABLE ? WITH CHECK CONSTRAINT all"

--Drop Column Id

--Go
--Exec sp_rename 'Product.Id_new', 'Id','Column'
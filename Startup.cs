using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.HttpsPolicy;
//using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
//using Microsoft.EntityFrameworkCore;
using WebShopRM.Models;
using WebShopRM.Helpers;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;

namespace WebShopRM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSession();

            //services.AddDistributedMemoryCache(); //20200309
            //services.AddSession(options =>
            //{
            //    options.Cookie.Name = ".webshopRM.session";
            //    // Set a short timeout for easy testing.
            //    options.IdleTimeout = TimeSpan.FromSeconds(10);
            //    options.Cookie.HttpOnly = true;
            //    // Make the session cookie essential
            //    options.Cookie.IsEssential = true;
            //});

            //services.AddControllersWithViews();
            services.AddRazorPages();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc();
            //  .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //.AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
            //.AddDataAnnotationsLocalization();

            services.AddDbContext<WebShopRMContext>(
                options =>
                    options.UseSqlServer(Configuration.GetConnectionString("WebShopRMContext")
                    )
                    ) ;

            services.AddTransient<IDeleteImages, DeleteImages>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) //IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            
            //app.UseHttpContextItemsMiddleware();
            
            app.UseCookiePolicy();

            var supportedCultures = new[]
            {
                new CultureInfo("sv-SE")
            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            { 
                DefaultRequestCulture = new RequestCulture("sv-SE"),
                //Formating numbers dates etc
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures,

                //sv.formatNumber(3.1415)
                // > 3,142
            });

            

            //app.UseMvc();
            //app.UseMvcWithDefaultRoute();

            app.UseRouting();

            app.UseSession();//use sessions after use routing, but before use endpoints
            //Todo:
            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                //If using MVC style (Core 3)
                //.MapControllerRoute("default", "{controller=Home}/{action=Index}");

            });
            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //            name: "default", template: "{controller=Products}/{action=OnGet}");
            //});
        }
    }
}

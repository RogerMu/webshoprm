﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShopRM.Models
{
    
    public class AddProductVM
    {
        [Required, StringLength(100), Display(Name = "Name")]
        public string ProductName { get; set; }

        [StringLength(100)]
        public string PartNumber { get; set; }
        [StringLength(50)]
        public string ISBN { get; set; }

        [Required, StringLength(10000), Display(Name = "Product Description"), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        //public string ImagePath { get; set; }

        [Display(Name = "Price")]
        //[DataType(DataType.Currency)]
        //[Column(TypeName = "decimal(18,2)")]
        public decimal? Price { get; set; }

        public int StockQuantity { get; set; }

        public bool Visible { get; set; }

        //[DisplayFormat(DataFormatString ="{0:yyyyMMdd HH:mm")]
        //public DateTime? Updated { get; set; }

        //public int? CategoryID { get; set; }

        public List<Category> AllCategories { get; set; }

        //var Categories = Context
        //addProductVM.AllCategories = allCategories;


        public ICollection<Picture> Picture { get; set; }
    }
}

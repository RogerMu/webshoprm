﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebShopRM.Pages.Pictures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

namespace WebShopRM.Models
{
    [BindProperties]
    public class PicturesViewModel 
    {
        //[Obsolete]
        //private IHostingEnvironment _environment;
        private IWebHostEnvironment _environment;

        //[Obsolete]
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1041:Provide ObsoleteAttribute message", Justification = "<Pending>")]
        public PicturesViewModel( IWebHostEnvironment env)//IHostingEnvironment environment
        {
            //_environment = environment;
            _environment = env;
        }

        //[BindProperty]
        public List<IFormFile> UploadFile { get; set; }
        //public int Id { get; set; }
        //[BindProperty]
        public string PicName { get; set; }
        //public string PicSizes { get; set; }
        //public string PicSizeOriginal { get; set; }
        //public string PicPath { get; set; }
        //public DateTime Updated { get; set; }

        
    }
    
}

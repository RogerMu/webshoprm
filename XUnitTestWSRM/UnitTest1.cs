using System;
using XUnitTestWSRM;
using WebShopRM;
using Xunit;

namespace XUnitTestWSRM
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            WebShopRM.Models.Product testProduct = new WebShopRM.Models.Product
            {
                ProductID = 55,
                ProductName = "Testproduct"
            };

            Assert.Equal( "Testroduct", testProduct.ProductName);


        }
    }
}
